# Digital Vikings Backend

## Requirements

- nodes >= 18

## Installation

```
git clone git@gitlab.com:digitalvikings/backend.git
cd backend
cp .env.example .env
yarn 
yarn strapi admin:create -e admin@test.de -p Test4711 -f firstname -l lastname
yarn dev
```

this creates a backend with admin credentials:

```
User: admin@test.de
Pass: Test4711
```

## Demo Data

you can import some Demo Data

```
yarn demo:import
```


Can be used to login at http://localhost:1337


