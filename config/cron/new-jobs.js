async function checkForNewJobs() {
  try {
    const entries = await strapi.entityService.findMany(
      "api::jobpool.jobpool",
      {
        populate: "*",
        filters: {
          mailSent: false,
        },
      }
    );
    var counter = 0;
    var url, html;
    for (let i = 0; i < entries.length; i++) {
      url = entries[i].html?.url;
      //      if(url) {
      //        html = getHtml(url)
      //      }
      //      if (url && !html) {
      //        continue
      //      }

      const entry = await strapi.entityService.update(
        "api::jobpool.jobpool",
        entries[i].id,
        {
          data: {
            normalizedTitle: filterTitle(entries[i].title),
            mailSent: true,
          },
        }
      );

      counter++;
    }
    if (counter > 0) {
      const emailTemplate = {
        subject: `${counter} neu Jobs gefunden`,
        text: `Hallo <%= user.firstname %>
            Es wurden ${counter} neue Stellenanzeigen gefunden!

            ${entries.map(
          (el) => `
            ${el.companyName}
            ${el.url}
            ${el.title}`
        )}

            Service von CROSS Solution
            `,
        html: `<h1>Hallo <%= user.firstname %></h1>
            <p>Es wurden ${counter} neue Stellenanzeigen gefunden!<p>
            <hr>
            <ol>
            ${entries.map(
          (el) =>
            `<li>${el.companyName}<br><a href="${el.url}">${el.title}</a></li>`
        )}
            </ol>
            <hr>
            Service von <a href="https://cross-solution.de">CROSS Solution</a>`,
      };

      await strapi.plugins["email"].services.email.sendTemplatedEmail(
        {
          to: "bleek@cross-solution.de",
          // from: is not specified, the defaultFrom is used.
          bcc: "bleek@cross-solution.de",
        },
        emailTemplate,
        {
          user: {
            firstname: "Lars",
            email: "bleek@cross-solution.de",
          },
        }
      );
      console.log("Send Mail");
    }
    console.log("Jobs parsed", counter);
  } catch (error) {
    console.error(error);
  }
}

function filterTitle(title) {
  var normalizedTitle = title.replace(/\s+/g, ' ');
  normalizedTitle = normalizedTitle.replace(/ \([mwdf][|\/,:][mwdf][|\/,:][mwdf]\)/g, '');
  normalizedTitle = normalizedTitle.replace(/ \(all genders\)/g, '');
  normalizedTitle = normalizedTitle.replace(/er\*in/g, 'er:in');
  normalizedTitle = normalizedTitle.replace(/er\*In/g, 'er:in');
  return normalizedTitle
}


exports.checkForNewJobs = checkForNewJobs;
