const https = require("node:https");
const fs = require("fs");
const path = require("node:path");

async function importJobs() {
  const data = await strapi.entityService.findMany(
    "api::feed-jw-job.feed-jw-job",
    {
      filters: { job: null },
      limit: 1,
    }
  );

  for (const row of data) {
    console.log(row);
    const job = await importJob(row);
  }

  console.log("import job successful!", data);
  jobDone();
}

async function importJob(job) {
  const company = await importCompany(job.firma, job.logos);
  strapi.entityService.create("api::job.job", {
    data: {
      company: company.id,
      title: job.titel,
      location: job.beschreibungOrt,
      feedJwJob: job.id,
      jobLink: job.link,
      applyLink: job.bewerbungslink,
      onlineDate: job.laufendesDatum,
      publishedAt: new Date(),
    },
  });
}

async function importCompany(company, logos) {
  const companyExists = await strapi.entityService.findMany(
    "api::company.company",
    {
      filters: { name: company },
    }
  );
  console.log(companyExists, company, logos);
  let result;
  if (companyExists.length > 0) {
    result = {
      id: companyExists[0].id,
    };
  } else {
    logo = await importLogo(logos);
    result = await strapi.entityService.create("api::company.company", {
      data: {
        name: company,
        publishedAt: new Date(),
        logo: logo,
      },
    });
  }
  return result;
}

async function importLogo(logos) {
  if (logos) {
    const logo = await tryDownloadLogo(logos.logo);
    const tmpFileName = logo.path;
    const uploadPlugin = strapi.plugin("upload").controller("content-api");
    const filePath = path.resolve(tmpFileName);
    const stats = fs.statSync(filePath);

    const companyLogo = await strapi.plugins.upload.services.upload.upload({
      data: {}, //mandatory declare the data(can be empty), otherwise it will give you an undefined error. This parameters will be used to relate the file with a collection.
      files: {
        path: filePath,
        name: tmpFileName,
        type: "image/png", // mime type of the file
        size: stats.size,
      },
    });
    console.log(companyLogo);
    return companyLogo;
  }
}

function jobDone() {
  feedJwJobStatusStarted = false;
}

async function tryDownloadLogo(logo) {
  const tmpFileName = "logo_tmp";
  var file = fs.createWriteStream(tmpFileName);

  let downloader = new Promise((resolve, reject) => {
    https
      .get(logo, function (response) {
        response.pipe(file);
        file.on("error", (e) => {
          console.log("Download error");
          console.log("====", e);
          reject(e);
        });
        file.on("finish", function () {
          file.close();
          resolve(file);
        });
      })
      .on("error", (e) => {
        console.log("Download error2");
        reject(e);
      });
  });

  return await downloader;
}

exports.importJobs = importJobs;
