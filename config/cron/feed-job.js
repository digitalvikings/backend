// path: ./config/cron-tasks.js
//
//  *    *    *    *    *    *
//  ┬    ┬    ┬    ┬    ┬    ┬
//  │    │    │    │    │    |
//  │    │    │    │    │    └ day of week (0 - 7) (0 or 7 is Sun)
//  │    │    │    │    └───── month (1 - 12)
//  │    │    │    └────────── day of month (1 - 31)
//  │    │    └─────────────── hour (0 - 23)
//  │    └──────────────────── minute (0 - 59)
//  └───────────────────────── second (0 - 59, OPTIONAL)

const { parseHugeXml } = require("../../pkg/parseHugeXml")
const https = require('node:https');
const fs = require('fs');
const tmpFileName = "tmp_jobs.xml"

var feedJwJobStatusStarted = false;

async function feedJwJob(strapi) {
  let xmlFileUrl = strapi.config.get('server.feedJwJob.xmlFileUrl')
  if (xmlFileUrl === "") {
    console.warn("feedJwJob need config env: FJJ_XML_FILE_URL")
    return
  }
  if (feedJwJobStatusStarted) {
    return
  }
  console.log('start feed jwt job, xmlFileUrl is', xmlFileUrl)
  feedJwJobStatusStarted = true
  // download file
  try {
    await tryDownloadXmlFile(xmlFileUrl);
  } catch (e) {
    console.log("download xml file error", e)
    jobDone()
    return
  }

  let parsedXmlData = [];
  try {
    parsedXmlData = await parseXmlFile();
  } catch (e) {
    console.error("parse xml file error", e)
    jobDone()
    return
  }
  if (parsedXmlData.length === 0) {
    console.warn("parsed xml data is empty")
    jobDone()
    return
  }
  try {
    await importXmlData(strapi, parsedXmlData)
  } catch (e) {
    console.error("import xml data error", e)
    jobDone()
    return
  }
  console.log('feed jwt job successful!')
  jobDone()
}

function jobDone() {
  fs.unlinkSync(tmpFileName)
  feedJwJobStatusStarted = false
}

async function tryDownloadXmlFile(xmlFileUrl) {
  console.log('start download xml file')
  var file = fs.createWriteStream(tmpFileName);
  let downloader = new Promise((resolve, reject) => {
    https.get(xmlFileUrl, function (response) {
      response.pipe(file);
      file.on('error', (e) => {
        console.log('====', e)
        reject(e)
      })
      file.on('finish', function () {
        file.close();
        resolve(true)
      });
    }).on("error", e => {
      reject(e)
    });
  })

  return await downloader;
}

async function parseXmlFile() {
  let parser = new parseHugeXml(tmpFileName, 'anzeige');
  return await parser.parseAllNodes()
}

const dataFieldMap = {
  id: "jwID",
  interneId: "interneId",
  productBR: "productBR",
  titel: "titel",
  firma: "firma",
  logos: "logos",
  beschreibungOrt: "beschreibungOrt",
  regionen: "regionen",
  adressen: "adressen",
  kennziffer: "kennziffer",
  link: "link",
  bewerbungslink: "bewerbungslink",
  laufendesDatum: "laufendesDatum",
  volltext: "volltext",
  kategorisierung: "kategorisierung",
};

const fieldStrings = ['jwID', 'interneId', 'kennziffer', 'beschreibungOrt']
function convertXmlDataToModelData(xmlData) {
  let modelData = {}
  for (fieldKey in dataFieldMap) {
    let fieldValue = xmlData.anzeige[fieldKey];
    if (fieldStrings.indexOf(dataFieldMap[fieldKey]) >= 0) {
      fieldValue = fieldValue + "";
    }
    if (dataFieldMap[fieldKey] === 'laufendesDatum') {
      fieldValue = dateParse(fieldValue)
    }
    modelData[dataFieldMap[fieldKey]] = fieldValue
  }
  // todo if collection need publish
  modelData.publishedAt = new Date();
  return modelData
}

function dateParse(dateString) {
  // 30.09.2023 d.m.Y  can't use Date.parse
  let ymd = dateString.split(".")
  if (ymd.length === 3) {
    return new Date(ymd[2], ymd[1], ymd[0])
  } else {
    console.warn("unsupport date format")
    return new Date()
  }
}
async function importXmlData(strapi, xmlDatas) {
  for (let i = 0; i < xmlDatas.length; i++) {
    console.log('data import progress :', i + '/' + xmlDatas.length)
    let modelData = convertXmlDataToModelData(xmlDatas[i])
    let existModels = await strapi.entityService.findMany('api::feed-jw-job.feed-jw-job', {
      fields: ['id', 'jwID'],
      filters: { 'jwID': modelData.jwID },
    })
    let result;
    if (existModels.length > 0) {
      result = await strapi.entityService.update('api::feed-jw-job.feed-jw-job', existModels[0].id, { data: modelData })
    } else {
      result = await strapi.entityService.create('api::feed-jw-job.feed-jw-job', { data: modelData })
    }
  }
  return true
}


exports.feedJwJob = feedJwJob
