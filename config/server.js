const { feedJwJob } = require("./cron/feed-job");
const { importJobs } = require("./cron/import-jobs");
const { checkForNewJobs } = require("./cron/new-jobs");

module.exports = ({ env }) => ({
  host: env("HOST", "0.0.0.0"),
  port: env.int("PORT", 1337),
  app: {
    keys: env.array("APP_KEYS", ["testKey1", "testKey2"]),
  },
  url: env("STRAPI_PUBLIC_URL", "http://127.0.0.1:1337"),
  webhooks: {
    populateRelations: env.bool("WEBHOOKS_POPULATE_RELATIONS", false),
  },
  feedJwJob: {
    xmlFileUrl: env("FJJ_XML_FILE_URL", ""),
  },
  cron: {
    enabled: true,
    tasks: {
      // every 10 minutes
      "*/10 * * * *": () => {
        checkForNewJobs();
      },
      // every 10 minutes
      "*/10 * * * *": ({ strapi }) => {
        importJobs(strapi);
      },
      // once a day at 02:15 AM
      "15 2 * * *": ({ strapi }) => {
        feedJwJob(strapi);
      },
    },
  },
});
