const path = require("path");

module.exports = ({ env }) => ({
  email: {
    config: {
      provider: "nodemailer",
      providerOptions: {
        host: env("EMAIL_SMTP_HOST", "mc.cross-solution.de"), //SMTP Host
        port: env("EMAIL_SMTP_PORT", 465), //SMTP Port
        secure: env("EMAIL_SMTP_SECURE", true),
        auth: {
          user: env("EMAIL_SMTP_USERNAME", ""),
          pass: env("EMAIL_SMTP_PASSWORD", ""),
        },
        rejectUnauthorized: true,
        requireTLS: true,
      },
      settings: {
        defaultFrom: env("EMAIL_SETTINGS_FROM", ""),
        defaultReplyTo: env("EMAIL_SETTINGS_REPLYTO", ""),
      },
    },
  },
  documentation: {
    enabled: true,
    config: {
      info: {
        version: "1.0.0",
        title: "Scrapy Store API",
        description: "Backend for crawled jobpostings",
        contact: {
          name: "Contact",
          email: "bleek@cross-solution.de",
          url: "https://dv.cross-solution.de",
        },
      },
      "x-strapi-config": {
        // Default
        plugins: ["upload", "users-permissions", "async-mail"],
      },
    },
  },
  bfa: {
    enabled: true,
    config: {
      salaryEnabled: env.bool("BFA_SALARY_ENABLED", false),
      scanLimit: env.int("SCAN_LIMIT", 1),
      apiKey: env("BFA_API_KEY", "infosysbub-ega"),
      apiUrl: env(
        "BFA_API_URL",
        "https://rest.arbeitsagentur.de/infosysbub/entgeltatlas/pc/v1/entgelte/"
      ),
    },
  },
  listmonk: {
    enabled: true,
    config: {
      apiUrl: env("LISTMONK_API"),
      apiToken: env("LISTMONK_TOKEN"),
      lists: env.json("LISTMONK_LISTS"),
    },
  },
  "field-uuid": {
    enabled: true,
    config: {}
  }
});
