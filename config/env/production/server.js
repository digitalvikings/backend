const { feedJwJob } = require("../../cron/feed-job");
const { importJobs } = require("../../cron/import-jobs");
const { checkForNewJobs } = require("../../cron/new-jobs");

module.exports = () => ({
  cron: {
    enabled: true,
    tasks: {
      // once a minute
      "* * * * *": () => {
        checkForNewJobs();
      },
      // every 10 seconds
      "*/10 * * * * *": ({ strapi }) => {
        importJobs(strapi);
      },
      // once a day
      "0 48 6 * * *": ({ strapi }) => {
        feedJwJob(strapi);
      },
    }
  },
});
