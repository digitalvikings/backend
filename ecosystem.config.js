const REPO = 'https://gitlab.com/digitalvikings/backend.git';

module.exports = {
  apps : [{
    name: '@dv/backend',
    script: 'yarn',
    args: 'start',
    interpreter: '/bin/bash',
    env_production: {
      NODE_ENV: "production",
      PORT: 3001
    }
  }],
  
  deploy : {
    production: {
      user : 'dv',
      host : 'dv.cross-solution.de',
      ref  : 'origin/main',
      repo : REPO,
      path : '/home/dv/dv.cross-solution.de',
      'pre-deploy-local' : 'rsync -a --delete build/ dv@dv.cross-solution.de:dv.cross-solution.de/source/build/',
      'post-deploy' : 'cd /home/dv/dv.cross-solution.de/source/ && git pull && yarn && PATH="$PATH:~/.yarn/bin" pm2 startOrRestart ecosystem.config.js --interpreter bash --env production'
    },
  }
};
