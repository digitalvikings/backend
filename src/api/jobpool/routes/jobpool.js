'use strict';

/**
 * jobpool router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::jobpool.jobpool');
