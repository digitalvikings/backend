'use strict';

/**
 * jobpool service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::jobpool.jobpool');
