'use strict';

/**
 * jobpool controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::jobpool.jobpool');
