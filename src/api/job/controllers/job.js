'use strict';

/**
 * job controller
 */

const { createCoreController } = require('@strapi/strapi').factories;
const _ = require("lodash");

module.exports = createCoreController("api::job.job", ({ strapi }) => ({
  async sendmail(ctx) {
    const emailTemplate = {
      subject: "Anfrage Schaltpartner von <%= user.name %>",
      text: `Neue Anfrage von:
        <%= user.name %>
        <%= user.company %>
        <%= user.email %>
        <%= user.phone %>
        <%= user.contact %>
        <%= user.text %>`,
      html: `<h1>Kontakt über Schaltpartner</h1>
        <p>
        Name: <%= user.name %><br>
        Firma: <%= user.company %><br>
        Email: <%= user.email %><br>
        Phone: <%= user.phone %><br>
        Contact: <%= user.contact %><br>
        <h2>Nachricht</h2>
        <%= user.text %>.<p>`,
    };

    const to =
      ctx.request.body.develop === "1"
        ? "develop@schaltpartner.com"
        : "anfragen@schaltpartner.com";

    const user = {
      name: ctx.request.body.name ?? "",
      company: ctx.request.body.company ?? "",
      contact: ctx.request.body.contact ?? "",
      email: ctx.request.body.email ?? "",
      phone: ctx.request.body.phone ?? "",
      text: ctx.request.body.text ?? "",
    };

    const result = await strapi.plugins[
      "email"
    ].services.email.sendTemplatedEmail(
      {
        to: to,
      },
      emailTemplate,
      {
        user: _.pick(user, [
          "name",
          "company",
          "contact",
          "phone",
          "email",
          "text",
        ]),
      }
    );
    return { result };
  },
}));
