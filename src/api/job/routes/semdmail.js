module.exports = {
  routes: [
    {
      // Path defined with an URL parameter
      method: "POST",
      path: "/sendmail",
      handler: "job.sendmail",
    },
  ],
};
