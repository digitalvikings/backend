'use strict';

/**
 * feed-jw-job controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::feed-jw-job.feed-jw-job');
