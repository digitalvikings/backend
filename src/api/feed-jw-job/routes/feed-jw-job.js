'use strict';

/**
 * feed-jw-job router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::feed-jw-job.feed-jw-job');
