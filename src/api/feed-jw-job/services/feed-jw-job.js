'use strict';

/**
 * feed-jw-job service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::feed-jw-job.feed-jw-job');
